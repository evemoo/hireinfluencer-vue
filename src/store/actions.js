import Vue from 'vue';

import Vuex from 'vuex';
Vue.use(Vuex);
import axios from 'axios';


import VueResource from 'vue-resource'
Vue.use(VueResource);

// Vue.http.options.root = 'http://localhost:3000/';
var rooturl = 'http://localhost:8000/api/auth/';

Vue.http.options.root = rooturl;

export default {    

    get_rooturl({commit, state, rootState}, object) {
        return rooturl;
    },
}
