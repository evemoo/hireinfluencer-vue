//import the vue instance
import Vue from 'vue'
//import the App component
import App from './App'
//import the vue router
import VueRouter from 'vue-router'
import Meta from 'vue-meta'

import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

import  { store }  from './store/';

//tell vue to use the router
Vue.use(VueRouter)
Vue.use(Meta)
/* eslint-disable no-new */

import VueResourse from 'vue-resource';
Vue.use(VueResourse);

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import VueScrollTo from 'vue-scrollto';
 
Vue.use(VueScrollTo, {
     container: "body",
     duration: 500,
     easing: "ease-in",
     offset: 0,
     cancelable: true,
     onStart: false,
     onDone: false,
     onCancel: false,
     x: false,
     y: true
 })

import Home from './components/Home'
import auth from './components/auth'
import service from './components/service'
import service_view from './components/service_view'
import service_view_list from './components/service_view_list'
import search_brand from './components/search_brand'
import add_service1 from './components/add_service1'
import add_service2 from './components/add_service2'
import add_service3 from './components/add_service3'
import add_service4 from './components/add_service4'
import add_service5 from './components/add_service5'
import buy_service1 from './components/buy_service1'
import buy_service2 from './components/buy_service2'
import buy_service3 from './components/buy_service3'
import brand_profile from './components/brand_profile'
import shortlist from './components/shortlist'
import order_brand from './components/order_brand'
import my_projects from './components/my_projects'
import my_projects_detail from './components/my_projects_detail'
import login from './components/login'
import signup from './components/signup'
import signup2 from './components/signup2'
import signup3 from './components/signup3'

import headerCommon from './components/headerCommon'
import footerCommon from './components/footerCommon'
//define your routes
const routes = [
	{
		name: 'Home',
		path: '/',
		components: {
			default: Home,
			header: headerCommon,
			footer: footerCommon
		}
	},	
	{
		name: 'auth',
		path: '/auth',
		components: {
			default: auth,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'service',
		path: '/service',
		components: {
			default: service,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'service_view',
		path: '/service_view',
		components: {
			default: service_view,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'service_view_list',
		path: '/service_view_list',
		components: {
			default: service_view_list,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'search_brand',
		path: '/search_brand',
		components: {
			default: search_brand,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'add_service1',
		path: '/add_service1',
		components: {
			default: add_service1,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'add_service2',
		path: '/add_service2',
		components: {
			default: add_service2,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'add_service3',
		path: '/add_service3',
		components: {
			default: add_service3,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'add_service4',
		path: '/add_service4',
		components: {
			default: add_service4,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'add_service5',
		path: '/add_service5',
		components: {
			default: add_service5,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'buy_service1',
		path: '/buy_service1',
		components: {
			default: buy_service1,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'buy_service2',
		path: '/buy_service2',
		components: {
			default: buy_service2,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'buy_service3',
		path: '/buy_service3',
		components: {
			default: buy_service3,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'brand_profile',
		path: '/brand_profile',
		components: {
			default: brand_profile,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'shortlist',
		path: '/shortlist',
		components: {
			default: shortlist,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'order_brand',
		path: '/order_brand',
		components: {
			default: order_brand,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'my_projects',
		path: '/my_projects',
		components: {
			default: my_projects,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'my_projects_detail',
		path: '/my_projects_detail',
		components: {
			default: my_projects_detail,
			header: headerCommon,
			footer: footerCommon
		}
	},
	{
		name: 'login',
		path: '/login',
		components: {
			default: login
		}
	},
	{
		name: 'signup',
		path: '/signup',
		components: {
			default: signup
		}
	},
	{
		name: 'signup2',
		path: '/signup2',
		components: {
			default: signup2
		}
	},
	{
		name: 'signup3',
		path: '/signup3',
		components: {
			default: signup3
		}
	},	
]

// Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
	routes, // short for routes: routes
	mode: 'history'
})



//instatinat the vue instance
new Vue({
	//define the selector for the root component
	el: '#app',
	//pass the template to the root component
	template: '<App/>',
	//declare components that the root component can access
	components: { App },
	//pass in the router to the vue instance
	router,
	store
}).$mount('#app')//mount the router on the app
